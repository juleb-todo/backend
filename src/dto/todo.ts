import { Expose, Type } from 'class-transformer';
import {
  IsBoolean,
  IsDate,
  IsNotEmpty,
  IsOptional,
  IsString,
  MaxLength,
} from 'class-validator';

export class TodoQuery {
  @IsString()
  @IsOptional()
  search: string;
}

export class AddTodoDto {
  @IsString()
  @IsNotEmpty()
  @MaxLength(100)
  @Expose()
  title: string;

  @IsDate()
  @Type(() => Date)
  @Expose()
  dueDate: Date;
}

export class UpdateTodoDto {
  @IsString()
  @IsOptional()
  @MaxLength(100)
  @Expose()
  title?: string;

  @IsDate()
  @IsOptional()
  @Type(() => Date)
  @Expose()
  dueDate?: Date;

  @IsBoolean()
  @IsOptional()
  @Expose()
  completed?: boolean;
}
