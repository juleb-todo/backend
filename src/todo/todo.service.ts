import { Injectable } from '@nestjs/common';
import { NotFoundException } from '@nestjs/common/exceptions';
import { InjectRepository } from '@nestjs/typeorm';
import { plainToInstance } from 'class-transformer';
import { AddTodoDto, TodoQuery, UpdateTodoDto } from 'src/dto/todo';
import { ILike, Repository } from 'typeorm';
import { TodoEntity } from './todo.entity';

@Injectable()
export class TodoService {
  constructor(
    @InjectRepository(TodoEntity) private todoRepo: Repository<TodoEntity>,
  ) {}

  async getAll(query: TodoQuery) {
    return await this.todoRepo.find({
      where: { title: ILike(`%${query.search}%`) },
      order: {
        createdAt: 'DESC',
      },
    });
  }

  async getById(id: string) {
    const foundTodo = await this.todoRepo.findOne({
      where: { id },
    });

    if (!foundTodo) throw new NotFoundException('Todo not found');

    return foundTodo;
  }

  async add(dto: AddTodoDto) {
    const newTodo = plainToInstance(AddTodoDto, dto, {
      excludeExtraneousValues: true,
    });
    return await this.todoRepo.save(newTodo);
  }

  async update(id: string, dto: UpdateTodoDto) {
    const foundTodo = await this.getById(id);
    const newTodo = plainToInstance(UpdateTodoDto, dto);
    return await this.todoRepo.save({ ...foundTodo, ...newTodo });
  }

  async delete(id: string) {
    await this.getById(id);
    return await this.todoRepo.softDelete({ id });
  }
}
