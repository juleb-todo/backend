import { BaseEntity } from 'src/common/base.entity';
import { Column, Entity } from 'typeorm';

@Entity({ name: 'todo' })
export class TodoEntity extends BaseEntity {
  @Column({ type: 'varchar' })
  title: string;

  @Column({ type: 'date', name: 'due_date' })
  dueDate: Date;

  @Column({ type: 'boolean', default: false })
  completed: boolean;
}
