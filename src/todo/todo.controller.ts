import {
  Body,
  Controller,
  Get,
  Param,
  ParseUUIDPipe,
  Post,
  Patch,
  Delete,
  Query,
} from '@nestjs/common';
import { AddTodoDto, TodoQuery, UpdateTodoDto } from 'src/dto/todo';
import { TodoService } from './todo.service';

@Controller('todo')
export class TodoController {
  constructor(private todoService: TodoService) {}

  @Get()
  getTodoList(@Query() query: TodoQuery) {
    return this.todoService.getAll(query);
  }

  @Get(':id')
  getTodoById(@Param('id', ParseUUIDPipe) id: string) {
    return this.todoService.getById(id);
  }

  @Post()
  addTodo(@Body() dto: AddTodoDto) {
    return this.todoService.add(dto);
  }

  @Patch(':id')
  updateTodo(
    @Param('id', ParseUUIDPipe) id: string,
    @Body() dto: UpdateTodoDto,
  ) {
    return this.todoService.update(id, dto);
  }

  @Delete(':id')
  deleteTodo(@Param('id', ParseUUIDPipe) id: string) {
    return this.todoService.delete(id);
  }
}
